from HW3_opencv import OpenCV_Image, OpenCV_Video
import os
import cv2 as cv
import numpy as np


class CV_ImageProcess(OpenCV_Image):
    # img = np.unit8 array, shape (rows, columns, color_channels)

    def __init__(self, data=None, path_to_image=None, image_read_mode=None, threshold=False):
        super().__init__(path_to_image, image_read_mode)
        if data is not None:
            self.img = data
        self._t_hold = threshold

    def return_ROI(self):

        select_roi = self.select_ROI()
        roi_data = self.img[select_roi[1]:select_roi[1] + select_roi[3], select_roi[0]:select_roi[0] + select_roi[2]]
        roi_cv = CV_ImageProcess(data=roi_data)
        return roi_cv

    def blur(self, k, mode):
        """
        Blurs self.img
        :param k: kernel size (positive, odd)
        :param mode: ['h', 'gauss', 'med', 'bi']
        :return: new CV_ImageProcess object
        """
        blured_array = None
        if mode == 'h':
            homogeneous_blur = cv.blur(self.img, ksize=(k, k))
            blured_array = homogeneous_blur
        # kernel size (w, h) can be any positive integer values
        elif mode == 'gauss':
            gaussian_blur = cv.GaussianBlur(self.img, ksize=(k, k), sigmaX=0)
            blured_array = gaussian_blur
        # 0 implies that sigmaX is calculated using
        # kernel size (w, h) must be odd positive integer values
        elif mode == 'med':
            median_blur = cv.medianBlur(self.img, k)
            blured_array = median_blur
        # kernel size is a square window, must be odd
        elif mode == 'bi':
            bilateral_blur = cv.bilateralFilter(self.img, k, k * 2, k / 2)
            blured_array = bilateral_blur
        if blured_array is None:
            return None
        else:
            out_image = CV_ImageProcess(data=blured_array)
            return out_image

    def erode(self, shape, size):
        """
        OpenCV_Image erode handler

        :param shape: ['rect', 'cross', 'ellipse']
        :param size: Positive integer
        :return: new CV_ImageProcess object
        """
        cv_shape = None
        if shape == 'rect':
            cv_shape = cv.MORPH_RECT
        elif shape == 'cross':
            cv_shape = cv.MORPH_CROSS
        elif shape == 'ellipse':
            cv_shape = cv.MORPH_ELLIPSE
        if cv_shape is None:
            return None
        else:
            element = cv.getStructuringElement(cv_shape, (2 * size + 1, 2 * size + 1), (size, size))
            erode_array = cv.erode(self.img, element)
            erode_img = CV_ImageProcess(data=erode_array)
            return erode_img

    def dilate(self, shape, size):
        """
        OpenCV_Image dilate handler

        :param shape: ['rect', 'cross', 'ellipse']
        :param size: Positive integer
        :return: new CV_ImageProcess object
        """
        cv_shape = None
        if shape == 'rect':
            cv_shape = cv.MORPH_RECT
        elif shape == 'cross':
            cv_shape = cv.MORPH_CROSS
        elif shape == 'ellipse':
            cv_shape = cv.MORPH_ELLIPSE
        if cv_shape is None:
            return None
        else:
            element = cv.getStructuringElement(cv_shape, (2 * size + 1, 2 * size + 1), (size, size))
            dilate_array = cv.dilate(self.img, element)
            erode_img = CV_ImageProcess(data=dilate_array)
            return erode_img

    def threshold(self, t_value, type, maxvalue):
        """
        Returns CV_ImageProcess object with self.img thresholded
        :param t_value: Threshold value for comparison
        :param type: 0-Binary, 1-Binary_Inverted, 2-Truncate, 3-Threshold_to_Zero, 4-Threshold_to_Zero_Inverted
        :param maxvalue: Value to set chosen pixels [depends on depth of color desired: (255:8bit), (1:Binary)]
        :return: CV_ImageProcesss object
        """

        shape = self.img.shape

        if len(shape) > 2:
            g_img = cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)
        else:
            g_img = self.img

        if type >= 0 and type <= 4:
            _, thresh_img = cv.threshold(g_img, t_value, maxvalue, type)

            out_ob = CV_ImageProcess(data=thresh_img, threshold=True)
            return out_ob
        else:
            return None

    def fill_circles(self):
        """
        Uses floodfill algorithm to fill enclosed objects in current self.img
        :return: CV_ImageProcess object
        """
        if self._t_hold:
            # create copy of image array to perform operations on
            original_threshold = self.img.copy()
            # retrieve shape of array
            h, w = original_threshold.shape

            # black out the top 20% and bottom 80% rows
            original_threshold[0:int(0.2 * h), :] = 0
            original_threshold[int(0.8 * h):, :] = 0
            # this is needed because of the text and black area at the top and bottom of the frame

            # 0 create variable img_array_floodfill as new array to floodfill
            img_array_floodfill = original_threshold.copy()

            # 1 create variable mask, use numpy.zeros() function
            # parameters for zeros():  shape, datatype
            # shape: (h+2, w+2) as a tuple.  IMPORTANT that size be 2 pixels more than image
            # datatype: np.uint8 (numpy unsigned integer 8 bit, same as opencv)

            mask = np.zeros((h + 2, w + 2), np.uint8)

            # 2 use cv.floodFill() function on img_array_floodfill
            # parameters:
            # image: img_array_floodfill
            # mask: mask
            # seedPoint: tuple,origin top corner: (0,0)
            # newValue: value to floodfill with: max value for 8 bit color

            cv.floodFill(img_array_floodfill, mask, (0, 0), 256)

            # 3 Invert floodfilled image, img_array_floodfill
            # create variable img_floodfill_inv: use cv.bitwise_not() with img_array_floodfill as parameter

            img_floodfill_inv = cv.bitwise_not(img_array_floodfill)

            # 4 create variable cv_obj_out:  a CV_ImageProcess object
            # Parameters for new object:
            # data set to img_floodfill_inv and threshold set to True
            cv_obj_out = CV_ImageProcess(data=img_floodfill_inv, threshold=True)
            # 5 return cv_obj_out
            return cv_obj_out
        else:
            print("Cannot use fill_circles on image, threshold first")


def split_video_main():
    full_video = OpenCV_Video("Microfluidic droplet generation using vacuum control_480p.mp4")

    fiji_conv = full_video.num_frames / 1720

    print(
        """
        Frames per second:  %s
        Number frames:  %s
        Columns:  %s 
        Rows: %s

        """ % (full_video.fps, full_video.num_frames, full_video.xpix, full_video.ypix)
    )
    print(["Frame Array shape:  ", full_video.frame_stack.shape])

    frame_isolation_dict = {
        "Blank": 0,
        "100 mBar": 528,
        "200 mBar": 856,
        "300 mBar": 1159,
        "400 mBar": 1439,
        "500 mBar": 1618
    }

    for frame_name, frame_number_fiji in zip(frame_isolation_dict.keys(), frame_isolation_dict.values()):
        cv_frame_num = int(frame_number_fiji * fiji_conv)
        frame = full_video.return_frame(cv_frame_num)
        frame.display_cv(frame_name)

        # 1 create variable image_process_obj: a CV_ImageProcesss with data set to frame.img
        image_process_obj = CV_ImageProcess(data=frame.img)
        # 2 create variable filled_bubbles:  a CV_ImageProcess object
        # use the fill_bubbles() function on a CV_ImageProcess object, from #1
        filled_bubbles = fill_bubbles(image_process_obj)
        # 3 save image using save_img() method of filled_bubbles object
        # parameter: string, ends with .jpg, contains frame_name, use full save path (optional)
        filled_bubbles.save_img('filled_bubbles_frame%s.jpg' % frame_number_fiji)


def fill_bubbles(image_process):
    """
    Handles all image processing for floodfill algorithm
    :param image_process: CV_ImageProcess object
    :return:
    """

    # 1: use the blur() method of image_process
    # create variable: blur_sharp
    # use a Bilateral filter with kernel size between 20 - 40

    blur_sharp = image_process.blur(21, 'bi')
    # how to display?

    # 2 use the dilate method of the blurred object blur_sharp
    # create variable: dilate_specs
    # use an elliptical shape and size 2

    dialate_specs = blur_sharp.dilate('ellipse', 2)

    # 3 use the blur method of the blurred-dilated object dilate_specs
    # create variable sharpen_dilate_blurNoise
    # use a Median filter with kernel size in [1,3,5,7]

    sharpen_dilate_blurNoise = dialate_specs.blur(3, 'med')

    # 4 use the threshold method of blurred-dilated-blurred object sharpen_dilate_blurNoise
    # create variable thresh
    # use a threshold value of 120-150
    # use Binary_Inverted mode
    # set max threshold value for 8-bit color

    thresh = sharpen_dilate_blurNoise.threshold(130, 1, 255)

    # 5 use the fill_circles method of the thresh object
    # create variable out_obj

    out_obj = thresh.fill_circles()

    # 6 return out_obj

    return out_obj


if __name__ == '__main__':
    split_video_main()
