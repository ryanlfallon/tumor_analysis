import numpy as np
import cv2
from matplotlib import pyplot as plt
import os

print(cv2.version.opencv_version)


class OpenCV_Image:


    def __init__(self, path_to_image=None, image_read_mode=None):
        """
        Creates opencv-img handling object
        :param path_to_image: path to image file
        :param image_read_mode: 0-Grayscale, 1-Color, -1-Unchanged
        """
        self.img = None
        if path_to_image != None:
            try:
                self.img = cv2.imread(path_to_image, image_read_mode)
            except Exception as e:
                print("Cannot read image:\n")
                print(e)

    def display_cv(self, window_name="Image"):
        """
        Displays image until key is pressed, only if img exists
        :param window_name: Named window as str option
        :return: None
        """
        if self.img is not None:
            cv2.imshow(window_name, self.img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
        else:
            print("Image not set")

    def save_img(self, out_file_path_name):
        """
        Saves image file to path with filename
        :param out_file_path_name: full path with file name
        :return: True if success, False else
        """
        if self.img is not None:
            try:
                cv2.imwrite(out_file_path_name, self.img)
            except Exception as e:
                print("Exception:\n")
                print(e)
                print("Cannot write image")
                return False
            return True
        else:
            print("Image does not exist")
            return False

    def display_plt(self):
        """
        Displays image using matplotlib, color conversion from bgr to rgb
        :return: None
        """
        rgb_im = cv2.cvtColor(self.img, cv2.COLOR_BGR2RGB)
        plt.imshow(rgb_im)
        plt.show()

    def select_ROI(self):
        """
        Select square Region of interest
        :return: region of interest [top_left_xpix, top_left_ypix, width, height]
        """
        r = cv2.selectROI("Select Square ROI then Spacebar", self.img, fromCenter=False, showCrosshair=False)
        cv2.destroyAllWindows()
        return r

    def return_border(self, border_pix_width):
        """
        Creates a border for current self.img; border type:  <CONSTAND RED BORDER>
        :param border_pix_width: integer # of pixels to add to image on all sides for border
        :return: new opencv_image object
        """
        border_constant_red = cv2.copyMakeBorder(self.img,
                                                 border_pix_width, border_pix_width, border_pix_width, border_pix_width,
                                                 cv2.BORDER_CONSTANT, value=[0, 0, 255])
        # copyMakeBorder( image_arr,
        # top, bottom, left right # padding
        # border_type, red color code (BGR) )

        new_image_ob = OpenCV_Image()  # create new object
        new_image_ob.img = border_constant_red  # fill object img data
        return new_image_ob

    def split_image(self):
        """
        Splits current self.img in half, by xpix
        :return: tuple of 2 OpenCV_Image objects
        """
        ypix, xpix, im_chan = self.img.shape

        x_mid_idx = int(xpix/2)

        im_left = OpenCV_Image()
        s1 = self.img[:, :x_mid_idx, :]
        im_left.img = s1
        print(im_left.img.shape)

        im_right = OpenCV_Image()
        im_right.img = self.img[:, x_mid_idx:, :]
        print(im_right.img.shape)

        return (im_left, im_right)

class OpenCV_Video:
    frame_stack = None
    xpix = None
    ypix = None
    num_frames = None
    fps = None

    def __init__(self, path_to_video=None):
        """
        Creates opencv-video handling object
        :param path_to_video: path to video file
        """
        if path_to_video != None:
            cap = cv2.VideoCapture(path_to_video)  # create import object
            self.xpix = int(cap.get(3))  # frame width
            self.ypix = int(cap.get(4))  # frame height
            self.fps = int(cap.get(5))  # vid fps
            self.num_frames = int(cap.get(7))  # numframes
            self.frame_stack = np.zeros(
                (int(self.num_frames), self.ypix, self.xpix, 3),
                # numpy array to store frames[frame_number, rows, cols, bgr]
                dtype=np.uint8)  # data type set same as opencv-image files
            for i in range(self.num_frames):
                ret, frame = cap.read()  # read frame from video
                self.frame_stack[i, :, :, :] = frame  # save frame to stack
            cap.release()  # close import object
            # print(self.frame_stack.shape)

    def return_frame(self, frame_idx):
        """
        Returns frame from video frame stack (0 indexed)
        :param frame_idx: frame # to return; 0 <= integer < self.num_frames
        :return: frame: single image of video stack as OpenCV_Image
        """
        if type(frame_idx) is int:  # check that is integer
            if (0 <= frame_idx) and (self.num_frames > frame_idx):  # check that int is within frames
                frame_data = self.frame_stack[frame_idx, :, :, :]  # remove frame from stack
                frame = OpenCV_Image()  # create empty opencv-image object
                frame.img = frame_data  # set image data
                return frame  # return opencv-object
        return None  # only if frame_idx does not meet reqs


def opencv_classes_test():
    """
    test function, imports 1 image and 1 video from local directory.
    Displays image in opencv and plt
    Displays Frame from video
    :return: None
    """
    print("Select Image")
    files = os.listdir()
    for i, file in enumerate(files):
        print(i, file)
    selection = input("Select Image Index\n")
    file_val = files[int(selection)]
    img = OpenCV_Image(os.path.join(os.getcwd(), file_val), 1)
    roi = img.select_ROI()
    print(roi)
    img.display_plt()
    img.display_cv("testwindow")
    print("Select Video")
    for i, file in enumerate(files):
        print(i, file)
    selection = input("Select Video Index\n")
    vid_val = files[int(selection)]
    vid = OpenCV_Video(os.path.join(os.getcwd(), vid_val))
    vid_frame = vid.return_frame(5)
    vid_frame.display_cv("Frame # 6")


def blend_images(opcv_img_1, weight_1, opcv_img_2, weight_2):
    """
    Blends two OpenCV_Image objects according to parameter weights
    :param opcv_img_1: OpenCV_Image object
    :param weight_1: weight to blend opcv_img_1
    :param opcv_img_2: OpenCV_Image object
    :param weight_2: weight to blend opcv_img_2
    :return: new OpenCV_Image object
    """

    blended_data = cv2.addWeighted(opcv_img_1.img, weight_1, opcv_img_2.img, weight_2, 0)
    blended_out_object = OpenCV_Image()
    blended_out_object.img = blended_data
    return blended_out_object


if __name__ == '__main__':
    opencv_classes_test()
