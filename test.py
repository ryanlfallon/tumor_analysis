from HW3_opencv import OpenCV_Image
from HW4 import CV_ImageProcess
import numpy as np
import cv2
import os
from PIL import ImageFont, Image, ImageDraw

def anlayze_scans(path_to_folder=None, path_to_saved_imgs_folder=None):
    """
    This function takes in a folder filled with brain scans puts all the images through some image analysis. The images
    in this function are eroded, smoothed, thresholded, drawn on, and saved into a new folder. The images in the
    folder will be determined to have a tumor or not have a tumor based on numpy analysis of their pixel values. If a
    tumor is present, the tumor region will be outlined in a colored box and the approximate area of the brain scan that
    tumor takes up will be displayed as well. If a tumor is not present, the image will be tagged with a label of
    "Tumor is not present"
    This function is used in conjunction with a dash app that will display the before image and the anlayzed image
    side by side.
    :param path_to_folder: folder path that contains all images to be analyzed
    :param: path_to_saved_imgs_folder: path to a folder where all analyzed images will be saved
    """
    # make output directory
    # outputDir = make_dir(path_to_save_file)
    inaccurate = 0
    accurate = 0
    for brain in os.listdir(path_to_folder):
        # makes path to image in folder
        brain_img = os.path.join(path_to_folder, brain)

        # create OpenCV Image object
        brain_scan = OpenCV_Image(brain_img, 0)
    
        # determine the amount of pixels to erode based on area
        pixel_erosion = calc_pixel_erosion(brain_scan)

        # create CV Image Process Object
        mod_brain_scan = CV_ImageProcess(brain_scan.img)

        # erode image with calculated erosion_pixel
        eroded_brain_scan = mod_brain_scan.erode('ellipse', int(pixel_erosion))

        # determine image threshold required for size of the image
        low_thresh = calc_image_threshold(eroded_brain_scan)

        # smooth the  image
        smoothed_brain_scan = eroded_brain_scan.blur(15, 'h')
        # smoothed_brain_scan.display_cv()
        # check if a tumor exists in image
        has_tumor = check_image_has_tumor(smoothed_brain_scan)
        # if image was found to contain a tumor
        if has_tumor:
            # threshold the image again to eliminate edge pixels
            area_scan = smoothed_brain_scan.threshold(50, 3, 0)

            # calculate brain area in image
            brain_pixel_area = calculate_img_area(area_scan)

            # threshold image so tumor is white and everything else is black
            threshold_brain_process = eroded_brain_scan.threshold(int(low_thresh), 0, 255)

            # finding the boundaries of the tumor
            x_start, x_end, y_start, y_end = find_tumor_pixel_range(threshold_brain_process)


            if (brain.__contains__('y') or brain.__contains__('Y')) \
                    and check_accuracy(x_start, x_end, y_start, y_end, threshold_brain_process):
                accurate += 1
                continue_processing = True
                print("accurate count ", accurate)
            else:
                inaccurate += 1
                continue_processing = False
                print("inaccurate count ", inaccurate)

            if continue_processing:
                # Calculate tumor area in image
                tumor_pixel_area = calculate_img_area(threshold_brain_process)

                # calculate the tumor percent
                tumor_percent = int((tumor_pixel_area / brain_pixel_area) * 100)

                # create a red border around the tumor
                outlined_brain_scan = create_red_border_img_on_grayscale(brain_scan, x_start, x_end, y_start, y_end, 3)

                # save the image to a temporary file
                outlined_brain_scan.save_img(os.path.join(os.getcwd(), "temp_img.jpg"))

                # create output string for tumor images
                s = 'Approximate tumor area: \n%s%% of brain area' % tumor_percent

                # mark up the image with the string
                saved_img = draw_on_image("temp_img.jpg", s)
                # saved_img.display_cv()
                # save image to the output file
                saved_img.save_img(os.path.join(path_to_saved_imgs_folder, brain))

        # else image was found not to contain an image
        else:
            if brain.__contains__('y') or brain.__contains__('Y'):
                inaccurate += 1
                print("inaccurate count ", inaccurate)
            else:
                accurate += 1
                print("accurate count ", accurate)
                # create output string for tumor images
                s = 'No tumor is present'

                # mark up the image with the string
                saved_img = draw_on_image(brain_img, s)
                # saved_img.display_cv()

                # save image to the output file
                saved_img.save_img(os.path.join(path_to_saved_imgs_folder, brain))

    percent_accuracy = accurate / (inaccurate + accurate) * 100
    print("Accuracy of code for folder %s is " % path_to_folder, percent_accuracy, " percent")

def check_image_has_tumor(cv_object):
    """
    Checks if an Image contains a pixel value above a certain threshold that is indicative of a tumor present.
    :param cv_object : the image object
    :return boolean: True if tumor is found; False if tumor is not found
    """
    img_col_max_values = np.amax(cv_object.img, axis=0)
    print(np.max(img_col_max_values))
    if np.max(img_col_max_values) < 120:
        return False
    return True


def calculate_img_area(data):
    """
    Finds the unique pixel values of an image and counts of those pixels. Sums the pixels values that are
    not 0 (black).
    :param data: CV Image object
    :return area: area of the non-black region of the pixel
    """
    values, counts = np.unique(data.img, return_counts=True)
    area = 0
    for i in range(len(counts) - 1):
        area += counts[i + 1]
    return area


def find_tumor_pixel_range(brain):
    """
    Finds the x range and y range for pixel values of an thresholded image, by checking the array of pixel values when
    it goes from 0 -> nonzero and nonzero -> 0
    :param brain: CV image object
    :return row_index_start: The start of the x range for the tumor pixels
    :return row_index_end: The end of the x range for the tumor pixels
    :return col_index_start:The start of the y range for the tumor pixels
    :return col_index_end:The end of the y range for the tumor pixels
    """
    img_col_max_values = np.amax(brain.img, axis=0)
    img_row_max_values = np.amax(brain.img, axis=1)
    row_index_start = 0
    while img_row_max_values[row_index_start] == 0:
        row_index_start += 1
    row_index_end = row_index_start
    while img_row_max_values[row_index_end] != 0:
        row_index_end += 1
    col_index_start = 0
    while img_col_max_values[col_index_start] == 0:
        col_index_start += 1
    col_index_end = col_index_start
    while img_col_max_values[col_index_end] != 0:
        col_index_end += 1
    return row_index_start, row_index_end, col_index_start, col_index_end


def calc_pixel_erosion(brain_scan):
    """
    Determins the amount of pixels that an image needs to be eroded based on the size of the entire image
    :param brain_scan: image to be eroded
    :return pixel_erosion: The amount of pixels that will be eroded in the image.
    """
    xpix, ypix = brain_scan.img.shape
    pixel_area = xpix*ypix
    if pixel_area < 100000:
        pixel_erosion = pixel_area * .0001
        kernel_size = 15
    if 100000 <= pixel_area < 1000000:
        pixel_erosion = (pixel_area * .0001) / 4
        kernel_size = 15
    if pixel_area > 1000000:
        pixel_erosion = pixel_area * .000001
        kernel_size = 15
    return pixel_erosion


def calc_image_threshold(img_data):
    """
    Determines the threshold for pixel values that will be turned to black by taking a percent of the max pixel value
    :param img_data: to determine threshold
    :return low_thresh: The lower limit of the threshold that will turn to black
    """
    values, counts = np.unique(img_data.img, return_counts=True)
    max_pixel = np.amax(values)
    low_thresh = .62 * max_pixel
    return low_thresh


def create_red_border_img_on_grayscale(gray_img, x1, x2, y1, y2, border_width):
    """
    Takes in a gray scale image and converts it to a RGB scale image and then adds a red border around the region that
    was input into the function at a desired width. The original image is then returned as an RGB scale image
    with an outline within the image
    :param gray_img: the gray scale image to be bordered.
    :param x1: the start of the x range to be bordered
    :param x2: the end of the x range to be bordered
    :param y1: the start of the y range to be bordered
    :param y2: the end of the y range to be bordered
    :param border_width: width of the border
    :return outline_tumor: image that has a a bordered around the desired region
    """

    bgr_brain_threshold = cv2.cvtColor(gray_img.img, cv2.COLOR_GRAY2BGR)
    tumor_region = bgr_brain_threshold[x1:x2, y1:y2, :]

    border_tumor = cv2.copyMakeBorder(tumor_region,
                                      border_width, border_width, border_width, border_width,
                                      cv2.BORDER_CONSTANT, value=[0, 0, 255])
    bgr_brain_threshold[x1 - border_width:x2 + border_width, y1 - border_width:y2 + border_width, :] = border_tumor

    outline_tumor = OpenCV_Image()
    outline_tumor.img = bgr_brain_threshold
    return outline_tumor


def draw_on_image(brain_img, output_string):
    """
    Marks the image with the input string
    :param brain_img: image to be drawn on
    :param: output_string: the string that will be drawn on the image
    :return saved_img: image that will be saved to the folder
    """
    img0 = Image.open(brain_img).convert('RGB')
    img1 = ImageDraw.Draw(img0)
    img1.text((5, 2), output_string, fill=(255, 240, 0))
    saved_img = OpenCV_Image()
    saved_img.img = np.asarray(img0)
    saved_img.img = cv2.cvtColor(saved_img.img, cv2.COLOR_RGB2BGR)
    return saved_img


def make_dir(folder_name):
    """
    Takes in a folder name and creates a folder path connected to the CWD. If it exist already, it returns the path,
     if not it makes the folder then returns the path
    :param folder_name : name of the folder
    :return folder_path: path to the folder
    """
    folder_path = os.path.join(os.getcwd(), folder_name)
    if os.path.exists(folder_path):
        return folder_path
    else:
        os.mkdir(folder_path)
        return folder_path
 
 
def check_accuracy(x1, x2, y1, y2, thresholded_image):
    """
    This function checks that the image being outlined is above a certain percent of the thresholded image. If it
    the bordered image outlines more than 90 percent of the image, it is considered an accurate outline, and it will
    be analyzed and saved into the correct folder. if not, it will not be anlayzed and will be counted as an inaccurate
    analysis.
    :param x1: the start of the x range to be bordered
    :param x2: the end of the x range to be bordered
    :param y1: the start of the y range to be bordered
    :param y2: the end of the y range to be bordered
    :param thresholded_image: thresholded image
    :return outline_tumor: image that has a a bordered around the desired region
    """
    tumor_region = thresholded_image.img[x1:x2, y1:y2]

    values, count1 = np.unique(tumor_region, return_counts=True)

    values2, count2 = np.unique(thresholded_image.img, return_counts=True)

    if count1.size > 1:
        count_outlined = count1[1]
    else:
        count_outlined = 0

    count_total = count2[1]

    percent_outlined = count_outlined/count_total

    if percent_outlined > .90:
        return True
    else:
        return False


if __name__ == '__main__':
    directory = 'assets'
    imgs_file = make_dir(directory)
    saved_folder = make_dir("Analyzed_Brain_scans")
    anlayze_scans(imgs_file, saved_folder)
    directory = 'no'
    no_scans = make_dir(directory)
    anlayze_scans(no_scans, saved_folder)
    directory = 'yes'
    yes_scans = make_dir(directory)
    anlayze_scans(yes_scans, saved_folder)
