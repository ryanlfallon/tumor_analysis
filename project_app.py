import dash
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
import flask
import glob
import os
from dash.dependencies import Output,Input
import plotly.express as px
from HW4 import CV_ImageProcess
import test

"""This code creates a Dash app where analyzed brain MRI images are displayed on a webpage.
   The images can be selected using a dropdown menu 
   The original image will be displayed on the left side of the screen and the analyzed image on the right"""

# creates paths to image directories
image_directory = os.path.join(os.getcwd(), "datasets")
raw_dir = os.path.join(image_directory, 'raw_image')
search = os.path.join(raw_dir, '*.jpg')
globreturn = glob.glob(search)
list_of_images = [os.path.basename(x) for x in globreturn]
analyzed_dir = os.path.join(image_directory, 'Analyzed_MRIs')
static_image_route = '/static/'

# runs analyze_scans function from test.py
test.anlayze_scans(raw_dir, analyzed_dir)


# creates list of analyzed images from the analyzed image directory with extension .jgp
list_of_analyzed_img = [os.path.basename(x) for x in glob.glob(os.path.join(analyzed_dir,'*.jpg'))]

# creates Dash app with the YETI theme for the webpage
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.YETI])

# creates headers and header descriptions for the Dash app
app.layout = html.Div(
    children=[
            html.Div(
                children=[
                    html.H1(children="Brain MRI Tumor Detection", className="header-title"),

                    html.P(
                        children="Use the Dropdown menu to select MRI images",
                        className="header-description",
                    ),
                ],
                className="header"),
            # creates dropdown menu for images
            html.Div([
                dcc.Dropdown(
                    id='image-dropdown',
                    options=[{'label': i, 'value': i} for i in list_of_images],
                    value=list_of_images[0]

                ),
                # uses rows and columns to place images on the Dash app
                # images are displayed using the dash bootstrap component Card
                dbc.Row(
                    [
                        dbc.Col(dbc.Card(
                            [
                                dcc.Graph(id='image'),
                                html.H2('Original Image', className = 'text-center')
                            ]
                        )),
                        dbc.Col(dbc.Card(
                            [
                                dcc.Graph(id='image_post'),
                                html.H2('Analyzed Image', className = 'text-center')
                            ]
                        ))
                    ])
            ]),
     ])

# creates app callback with two outputs: original image and analyzed image
# only takes one input from the dropdown menu
@app.callback(
    [Output('image','figure'), Output('image_post','figure') ],
    [Input('image-dropdown', 'value')])
# function to update the images when selected in the dropdown menu
# uses plotly express to show analyzed images
# returns the raw and analyzed figures
def update_image_src(value):
    raw_path = os.path.join(raw_dir, value)
    analyzed_path = os.path.join(analyzed_dir, value)
    raw_img = CV_ImageProcess(path_to_image=raw_path, image_read_mode=1)
    raw_fig = px.imshow(raw_img.img)
    analyzed_img = CV_ImageProcess(path_to_image= analyzed_path,image_read_mode=1 )
    analyzed_fig = px.imshow(analyzed_img.img)
    return raw_fig, analyzed_fig

# creates app server route
@app.server.route('{}<image_path>.jpg'.format(static_image_route))
def serve_image(image_path):
    image_name = '{}.jpg'.format(image_path)
    if image_name not in list_of_images:
        raise Exception('"{}" is excluded from the allowed static files'.format(image_path))
    return flask.send_from_directory(image_directory, image_name)

if __name__ == '__main__':
    app.run_server(debug=True)